// GENERATED AUTOMATICALLY FROM 'Assets/Inputs/Standard.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @Standard : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @Standard()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""Standard"",
    ""maps"": [
        {
            ""name"": ""DebugConsole"",
            ""id"": ""fa34fd33-ec46-4a7f-a924-ae80aea9a706"",
            ""actions"": [
                {
                    ""name"": ""ToggleDebug"",
                    ""type"": ""Button"",
                    ""id"": ""9d9ad871-8e64-4749-b09d-4c077e65375b"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Return"",
                    ""type"": ""Button"",
                    ""id"": ""c6d6f836-71ca-407d-bb39-bdf81b122830"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": """",
                    ""id"": ""499f2967-0dc8-42f8-bf12-ccc0499bbe01"",
                    ""path"": ""<Keyboard>/period"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""ToggleDebug"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""68382e7b-741e-430f-8dfe-386263649c30"",
                    ""path"": ""<Keyboard>/enter"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Return"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // DebugConsole
        m_DebugConsole = asset.FindActionMap("DebugConsole", throwIfNotFound: true);
        m_DebugConsole_ToggleDebug = m_DebugConsole.FindAction("ToggleDebug", throwIfNotFound: true);
        m_DebugConsole_Return = m_DebugConsole.FindAction("Return", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // DebugConsole
    private readonly InputActionMap m_DebugConsole;
    private IDebugConsoleActions m_DebugConsoleActionsCallbackInterface;
    private readonly InputAction m_DebugConsole_ToggleDebug;
    private readonly InputAction m_DebugConsole_Return;
    public struct DebugConsoleActions
    {
        private @Standard m_Wrapper;
        public DebugConsoleActions(@Standard wrapper) { m_Wrapper = wrapper; }
        public InputAction @ToggleDebug => m_Wrapper.m_DebugConsole_ToggleDebug;
        public InputAction @Return => m_Wrapper.m_DebugConsole_Return;
        public InputActionMap Get() { return m_Wrapper.m_DebugConsole; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(DebugConsoleActions set) { return set.Get(); }
        public void SetCallbacks(IDebugConsoleActions instance)
        {
            if (m_Wrapper.m_DebugConsoleActionsCallbackInterface != null)
            {
                @ToggleDebug.started -= m_Wrapper.m_DebugConsoleActionsCallbackInterface.OnToggleDebug;
                @ToggleDebug.performed -= m_Wrapper.m_DebugConsoleActionsCallbackInterface.OnToggleDebug;
                @ToggleDebug.canceled -= m_Wrapper.m_DebugConsoleActionsCallbackInterface.OnToggleDebug;
                @Return.started -= m_Wrapper.m_DebugConsoleActionsCallbackInterface.OnReturn;
                @Return.performed -= m_Wrapper.m_DebugConsoleActionsCallbackInterface.OnReturn;
                @Return.canceled -= m_Wrapper.m_DebugConsoleActionsCallbackInterface.OnReturn;
            }
            m_Wrapper.m_DebugConsoleActionsCallbackInterface = instance;
            if (instance != null)
            {
                @ToggleDebug.started += instance.OnToggleDebug;
                @ToggleDebug.performed += instance.OnToggleDebug;
                @ToggleDebug.canceled += instance.OnToggleDebug;
                @Return.started += instance.OnReturn;
                @Return.performed += instance.OnReturn;
                @Return.canceled += instance.OnReturn;
            }
        }
    }
    public DebugConsoleActions @DebugConsole => new DebugConsoleActions(this);
    public interface IDebugConsoleActions
    {
        void OnToggleDebug(InputAction.CallbackContext context);
        void OnReturn(InputAction.CallbackContext context);
    }
}
