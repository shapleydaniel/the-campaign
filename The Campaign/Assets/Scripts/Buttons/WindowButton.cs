﻿using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.EventSystems;
using UnityEngine.ResourceManagement.AsyncOperations;

public class WindowButton : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private string windowName;
    [SerializeField] private string dataKey;
    
    private object data;

    private int numberClicks;
    private float timeOnFirstClick;
    private float clickResponseTime = 1f;

    private bool buttonPressed;
    
    public void OnPointerClick(PointerEventData eventData)
    {
        //if (!CheckDoubleClick()) return;

        if (buttonPressed) return;
        buttonPressed = true;
        if (data == null && !string.IsNullOrEmpty(dataKey))
        {
            Addressables.LoadAssetAsync<ScriptableObject>(dataKey).Completed += OpenWindow;
        } else if (data != null)
        {
            buttonPressed = false;
            WindowController.OpenWindow(CheckNameCorrectFormat(windowName), data);
        }
        else
        {
            buttonPressed = false;
            WindowController.OpenWindow(CheckNameCorrectFormat(windowName));
        }
        
    }

    private void OpenWindow(AsyncOperationHandle<ScriptableObject> obj)
    {
        if (obj.Result == null) return;
        data = obj.Result;
        WindowController.OpenWindow(CheckNameCorrectFormat(windowName),obj.Result);
        buttonPressed = false;
    }

    public void SetButtonData<T>(T item)
    {
        data = item;
    }

    public void SetWindowName(string newWindow)
    {
        windowName = newWindow;
    }
    
    private bool CheckDoubleClick()
    {
        numberClicks++;
        if (numberClicks == 1) timeOnFirstClick = Time.time;
            
                
        if (numberClicks > 1 && Time.time - timeOnFirstClick < clickResponseTime)
        {
                numberClicks = 0;
                timeOnFirstClick = 0;
                return true;
        }

        if (numberClicks > 2 || Time.time - timeOnFirstClick > 4) numberClicks = 0;

        return false;
    }

    private string CheckNameCorrectFormat(string windowName)
    {
        windowName = windowName.ToLower();
        if (windowName.Contains("window"))
        {
            return windowName;
        }
        else
        {
            windowName += " window";
            return windowName;
        }
    }
}
