﻿using GameData;
using System;
using UnityEngine;

public class GameStats : MonoBehaviour
{
    private static GameStats instance;

    public VoteData votes;
    public FundsData funds;
    public TimeData time;

    private const float GameTime = 10f;
    public static GameStats Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<GameStats>();
            }

            return instance;
        }
    }

    public void Update()
    {
        UpdateTime();
    }

    private void UpdateTime()
    {
        time.time += Time.deltaTime * GameTime;
        if (!(time.time >= 1440)) return;
        time.time = 0;
        time.Date++;
    }

    public int ReturnDate()
    {
        if (time == null) return 0;
        return time.Date;
    }

    public float ReturnTime()
    {
        if (time == null) return 0;
        return time.time;
    }

    public int ReturnDateTime()
    {
        if (time == null) return 0;
        return time.Date + (int)time.time;
    }

    public float ReturnTotalFunds()
    {
        if (funds == null) return 0;
        return funds.total;
    }

    public float ReturnVotes()
    {
        if (votes == null) return 0;
        return votes.votes;
    }

    public TimeData ReturnTimeData()
    {
        if (time == null) return null;
        return time;
    }

    public void AdjustStats(string statName, int value)
    {
        switch (statName)
        {
            case "votes":
                votes.AddVotes(value);
                break;
            case "funds":
                funds.AddFunds(value);
                break;
        }
    }
}
