﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Party/Party")]
public class PartyData : ScriptableObject
{
    public string partyName;
    public Color partyColor;
    public float votes;
}
