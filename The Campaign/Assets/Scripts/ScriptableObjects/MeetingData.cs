using UnityEngine;

[CreateAssetMenu(menuName = "Meeting/Meeting")]
public class MeetingData : ScriptableObject
{
    public TextAsset script;
}
