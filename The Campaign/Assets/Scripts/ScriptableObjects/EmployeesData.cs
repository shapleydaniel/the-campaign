﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Employee/Employees")]
public class EmployeesData : ScriptableObject
{
    public List<EmployeeData> allEmployees;

    public List<EmployeeData> GetEmployeesAtLevel(int level)
    {
        if (allEmployees == null) return null;
        var availableEmployees = new List<EmployeeData>();
        foreach (var employee in allEmployees)
        {
            if(employee.skillLevel <= level && employee.available)
            {
                availableEmployees.Add(employee);
            }
        }

        return availableEmployees;
    }
}
