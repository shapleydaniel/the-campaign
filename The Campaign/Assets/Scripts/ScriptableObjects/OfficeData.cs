﻿using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "OfficeData", menuName = "The Campaign/OfficeData", order = 0)]
public class OfficeData : ScriptableObject 
{
    public int numberDesks;
    public List<EmployeeData> employees;

    public static Action<EmployeeData> HireEmployee;
    public static Action<EmployeeData> FireEmployee;

    private void OnEnable()
    {
        HireEmployee += AddEmployee;
        FireEmployee += RemoveEmployee;
    }

    private void OnDisable()
    {
        HireEmployee = null;
        FireEmployee = null;
    }

    private void AddEmployee(EmployeeData employee)
    {
        employees.Add(employee);
    }

    private void RemoveEmployee(EmployeeData employee)
    {
        employees.Remove(employee);
    }
}

