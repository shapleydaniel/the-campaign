﻿using System;
using System.Collections.Generic;
using GameData;
using UnityEngine;

[CreateAssetMenu(menuName = "Candidate/Candidate")]
public class CandidateData : ScriptableObject
{
    public Sprite idSprite;
    public PartyData party;
    public FundsData funds;
    public List<PreferenceData> preferences;
    public List<TaskData> tasks;

    private void Awake()
    {
        var allPreferences = Enum.GetValues(typeof(PreferenceType));
        preferences = new List<PreferenceData>();
        foreach (PreferenceType preference in allPreferences)
        {
            if (preference == PreferenceType.None) continue;
            preferences.Add(new PreferenceData{preferenceName = preference});
        }
    }
}
