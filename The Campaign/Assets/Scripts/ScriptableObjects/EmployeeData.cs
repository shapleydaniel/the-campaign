﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = ("Character/Employee"))]
public class EmployeeData : ScriptableObject
{
    public string employeeName;
    public int age;
    public Sprite idImage;
    public int skillLevel = 1;
    public List<TaskData> availableTasks = new List<TaskData>();
    public bool available = true;
    public List<TaskData> currentTasks = new List<TaskData>();

    public float TaskProgress
    {
        get {
            if(currentTasks.Count <= 0)
            {
                return 0;
            }

            return currentTasks[0].progress;
        }
    }

    private void OnEnable()
    {
        ResetEmployee();
    }

    public void AddTask(TaskData task)
    {
        currentTasks.Add(task);
    }

    public void ResetEmployee()
    {
        currentTasks.Clear();
    }

    public void RemoveTasks(List<TaskData> tasks)
    {
        foreach (var item in tasks)
        {
            currentTasks.Remove(item);
        }
    }
}

