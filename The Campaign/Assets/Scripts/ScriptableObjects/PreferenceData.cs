﻿using System;
using UnityEngine;

public enum PreferenceType
{
    None,
    Education,
    Military,
    Taxes,
    Guns
}

[Serializable]
public class PreferenceData
{
    public PreferenceType preferenceName;
    
    [Range(-1, 1)]
    public float weight;
    public Color color;
}
