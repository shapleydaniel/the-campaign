﻿using UnityEngine;

namespace GameData
{
    [CreateAssetMenu(menuName = "GameData/Votes")]
    public class VoteData : ScriptableObject
    {
        public int votes;

        public void AddVotes(int value)
        {
            votes += value;
        }
    }
}

