﻿using System.Collections.Generic;
using UnityEngine;

public class PlayerData : ScriptableObject
{
    public Dictionary<string, int> agreements = new Dictionary<string, int>();
}
