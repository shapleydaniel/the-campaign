﻿using UnityEngine;

[CreateAssetMenu(menuName = "Task/Task")]
public class TaskData : ScriptableObject
{
    [Range(1,100)]
    public float time;
    [Range(1,100)]
    public float difficulty;
    [Range(0,1)]
    public float progress = 1;
    
    public float funds;
    public Sprite icon;

    public void ResetProgress()
    {
        progress = 1;
    }
}
