﻿using System.Linq;
using UnityEngine;

namespace GameData
{
    [CreateAssetMenu(menuName = "GameData/Funds")]
    public class FundsData : ScriptableObject
    {
        public float[] days;
        public float[] weeks;
        public float[] months;

        public float total;

        public float[] GetFunds(FundStats type)
        {
            switch (type)
            {
                case FundStats.None:
                    return null;
                case FundStats.Day:
                    return days;
                case FundStats.Week:
                    return weeks;
                case FundStats.Month:
                    return months;
                default:
                    return null;
            }
        }

        public float UpdateTotal()
        {
            if (months.Length > 0)
            {
                return  total + months.Sum();
            }
        
            if(weeks.Length > 0)
            {
                return total + weeks.Sum();
            }

            return  total + days.Sum();
        }

        public void AddFunds(float funds)
        {
            days[GameStats.Instance.ReturnDate()] += funds;
        }
    }

    public enum FundStats
    {
        None,
        Day,
        Week,
        Month
    }
}