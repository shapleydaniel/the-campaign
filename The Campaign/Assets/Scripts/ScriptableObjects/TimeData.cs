﻿using System.Collections.Generic;
using UnityEngine;

namespace GameData
{
    [CreateAssetMenu(menuName = "GameData/Time")]
    public class TimeData : ScriptableObject
    {
        public int Date;
        public float time;

        public Dictionary<int, CampaignEvent> Events = new Dictionary<int, CampaignEvent>();

        public void NextDay()
        {
            Date++;
        }

        public void AddEvent(int date, CampaignEvent eventType)
        {
            Events.Add(date, eventType);
        }
    }
}
