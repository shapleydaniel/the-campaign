﻿using System.Collections.Generic;
using UnityEngine;

public class TabGroup : MonoBehaviour
{
   [SerializeField]private Color Normal;
   [SerializeField]private Color Highlight;
   [SerializeField]private Color Selected;
   
   private List<TabButton> tabButtons;
   private TabButton currentlySelected;
   
   public void Subscribe(TabButton button)
   {
      if (tabButtons == null)
      {
         tabButtons = new List<TabButton>();
      }
      
      tabButtons.Add(button);
   }

   public void OnTabEnter(TabButton button)
   {
      if (button == currentlySelected) return;
      button.background.color = Highlight;
   }

   public void OnTabExit(TabButton button)
   {
      if (button == currentlySelected) return;
      button.background.color = Normal;
   }

   public void OnTabSelected(TabButton button)
   {
      if (currentlySelected != null)
      {
         currentlySelected.background.color = Normal;
         currentlySelected.ActivateWindow(false);
      }
      
      button.background.color = Selected;
      currentlySelected = button;
      currentlySelected.ActivateWindow(true);
   }

   public void ResetTabs()
   {
      foreach (var button in tabButtons)
      {
         if (button == currentlySelected) continue;
         button.background.color = Normal;
      }
   }
}
