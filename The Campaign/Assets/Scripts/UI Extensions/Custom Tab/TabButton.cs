﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TabButton : MonoBehaviour, IPointerEnterHandler, IPointerClickHandler, IPointerExitHandler
{
   [SerializeField] private TabGroup tabGroup;
   public Image background;
   [SerializeField] private GameObject window;

   private void Start()
   {
      tabGroup.Subscribe(this);
   }
   
   public void OnPointerEnter(PointerEventData eventData)
   {
     tabGroup.OnTabEnter(this);
   }

   public void OnPointerClick(PointerEventData eventData)
   {
      tabGroup.OnTabSelected(this);
   }

   public void OnPointerExit(PointerEventData eventData)
   {
      tabGroup.OnTabExit(this);
   }

   public void ActivateWindow(bool active)
   {
      if (window == null)
      {
         Debug.Log($"Window not set in tab {gameObject.name}");
         return;
      }
      
      window.SetActive(active);
   }
}
