﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using Object = UnityEngine.Object;

namespace UI_Extensions
{
    public class Pool <T>
    {
        public AssetReference prefabReference;
        public Transform parent;
        public List<T> pool = new List<T>();

        private GameObject prefab;

        public bool initialised => pool.Count > 0;

        public void CreatePool(int maxPool, Action onComplete = null)
        {
            Addressables.LoadAssetAsync<GameObject>(prefabReference).Completed += op =>
            {
                if (op.Status != AsyncOperationStatus.Succeeded) return;
                prefab = op.Result;
                for (var i = 0; i < maxPool; i++)
                {
                    CreateItem();
                }

                onComplete?.Invoke();
            };
        }

        public T GetObject()
        {
            foreach (var item in pool)
            {
                if (item is Component { } itemObject && itemObject.gameObject.activeSelf) continue;
                return item;
            }

            return CreateItem();
        }

        public void ResetPool()
        {
            foreach (var itemObject in pool.Select(item => item as Component))
            {
                itemObject?.gameObject.SetActive(false);
            }
        }

        private T CreateItem()
        {
            if(prefab == null)
            {
                Debug.LogError($"Haven't initialised the pool");
                return default(T);
            }

            var itemObject = Object.Instantiate(prefab, parent);
            itemObject.SetActive(false);
            var item = itemObject.GetComponent<T>();
            pool.Add(item);
            return item;
        }
    }
}
