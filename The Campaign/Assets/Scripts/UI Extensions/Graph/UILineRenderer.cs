﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UILineRenderer : Graphic
{
    public Vector2Int gridSize;

    public List<Vector2> points;

    private float width;
    private float height;
    private float unitWidth;
    private float unitHeight;

    public float thickness = 10f;
    
    protected override void OnPopulateMesh(VertexHelper vh)
    {
        vh.Clear();
        var rect = rectTransform.rect;
        width = rect.width;
        height = rect.height;

        unitWidth = width / gridSize.x;
        unitHeight = height / gridSize.y;

        if (points.Count < 2) return;

        var angle = 0f;
        
        for (var i=0; i < points.Count; i++)
        {
            if (i < points.Count - 1)
            {
                angle = GetAngle(points[i], points[i + 1]) + 45f;
            }
            
            DrawVerticesForPoint(points[i], vh, angle);
           
        }

        for (var i = 0; i < points.Count-1; i++)
        {
            var index = i * 2;
            vh.AddTriangle(index+0, index + 1, index +3);
            vh.AddTriangle(index+3, index + 2, index +0);
        }

    }

    public float GetAngle(Vector2 position, Vector2 target)
    {
        return Mathf.Atan2(target.y - position.y, target.x - position.x) * (180 / Mathf.PI);
    }
    
    private void DrawVerticesForPoint(Vector2 point, VertexHelper vh, float angle)
    {
        var vertex = UIVertex.simpleVert;
        vertex.color = color;

        vertex.position = Quaternion.Euler(0,0,angle) * new Vector3(-thickness / 2, 0);
        vertex.position += new Vector3(unitWidth * point.x, unitHeight * point.y);
        vh.AddVert(vertex);
        
        vertex.position = Quaternion.Euler(0,0,angle) * new Vector3(thickness / 2, 0);
        vertex.position += new Vector3(unitWidth * point.x, unitHeight * point.y);
        vh.AddVert(vertex);

    }
}
