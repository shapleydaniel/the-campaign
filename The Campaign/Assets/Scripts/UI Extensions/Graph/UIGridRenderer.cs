﻿using UnityEngine;
using UnityEngine.UI;

public class UIGridRenderer : Graphic
{
    public Vector2Int gridSize = new Vector2Int(1, 1);
    public float thickness = 10f;

    private float width;
    private float height;
    private float cellWidth;
    private float cellHeight;
    
    protected override void OnPopulateMesh(VertexHelper vh)
    {
        vh.Clear();
        var rect = rectTransform.rect;
        width = rect.width;
        height = rect.height;

        cellWidth = width / gridSize.x;
        cellHeight = height / gridSize.y;

        var count = 0;
        for (var y = 0; y < gridSize.y; y++)
        {
            for (var x = 0; x < gridSize.x; x++)
            {
                DrawCell(x,y,count, vh);
                count++;
            }
        }
    }

    private void DrawCell(int x, int y, int index ,VertexHelper vh)
    {
        var xPos = cellWidth * x;
        var yPos = cellHeight * y;
        
        var vertex = UIVertex.simpleVert;
        vertex.color = color;
        
        vertex.position = new Vector3(xPos, yPos);
        vh.AddVert(vertex);
        
        vertex.position = new Vector3(xPos, yPos + cellHeight);
        vh.AddVert(vertex);
        
        vertex.position = new Vector3(xPos + cellWidth, yPos + cellHeight);
        vh.AddVert(vertex);
        
        vertex.position = new Vector3(xPos + cellWidth, yPos);
        vh.AddVert(vertex);

        var widthSqr = thickness * thickness;
        var distanceSqr = widthSqr / 2;
        var distance = Mathf.Sqrt(distanceSqr);
        
        vertex.position = new Vector3(xPos + distance, yPos + distance);
        vh.AddVert(vertex);
        
        vertex.position = new Vector3(xPos + distance, yPos + (cellHeight - distance));
        vh.AddVert(vertex);
        
        vertex.position = new Vector3(xPos + (cellWidth - distance), yPos + (cellHeight - distance));
        vh.AddVert(vertex);
        
        vertex.position = new Vector3(xPos + (cellWidth - distance), yPos + distance);
        vh.AddVert(vertex);

        //Each cell has 8 vertices, hence magic 8
        var offSet = index * 8;
        
        //Left Edge
        vh.AddTriangle(offSet + 0, offSet + 1, offSet + 5);
        vh.AddTriangle(offSet + 5, offSet + 4, offSet + 0);
        
        //Top Edge
        vh.AddTriangle(offSet + 1,offSet + 2,offSet + 6);
        vh.AddTriangle(offSet + 6,offSet + 5,offSet + 1);
        
        //Right Edge
        vh.AddTriangle(offSet + 2,offSet + 3,offSet + 7);
        vh.AddTriangle(offSet + 7,offSet + 6,offSet + 2);
        
        //Bottom Edge
        vh.AddTriangle(offSet + 3,offSet + 0,offSet + 4);
        vh.AddTriangle(offSet + 4,offSet + 7,offSet + 3);
    }
    
}
