﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class PieGraph : MonoBehaviour
{
    public GameObject webgePrefab;
    public float padding;
    
    public void MakeChart(CandidateData[] candidates)
    {
        if (candidates.Length <= 0) return;
        var total = CalculateTotalVotes(candidates);
        var zRotation = 0f;
        foreach (var candidate in candidates)
        {
            var newWedge = Instantiate(webgePrefab, transform).GetComponent<Image>();
            newWedge.fillAmount = candidate.party.votes / total;
            newWedge.transform.rotation = Quaternion.Euler(new Vector3(0,0, zRotation));
            zRotation -= newWedge.fillAmount * 360;
            newWedge.fillAmount -= padding;
            newWedge.gameObject.transform.SetSiblingIndex(0);
            newWedge.color = candidate.party.partyColor;
        }
    }

    private float CalculateTotalVotes(CandidateData[] candidates)
    {
        return candidates.Sum(candidate => candidate.party.votes);
    }
}
