﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ColorCode : MonoBehaviour
{
    [SerializeField] private Image icon;
    [SerializeField] private TextMeshProUGUI textMeshProUGUI;

    public void SetDetails(CandidateData candidateData)
    {
        icon.color = candidateData.party.partyColor;
        textMeshProUGUI.text = candidateData.party.name;
    }
}
