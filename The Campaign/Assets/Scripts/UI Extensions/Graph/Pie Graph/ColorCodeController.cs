﻿using UnityEngine;

public class ColorCodeController : MonoBehaviour
{
    [SerializeField] private ColorCode colorCodePrefab;

    public void CreateCode(CandidateData[] candidates)
    {
        foreach (var candidate in candidates)
        {
            var code = Instantiate(colorCodePrefab, transform);
            code.SetDetails(candidate);
        }
    }
}
