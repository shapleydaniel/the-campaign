﻿using UnityEngine;

public class PieGraphController : MonoBehaviour
{
    [SerializeField] private string candidatesFolder;
    [SerializeField] private ColorCodeController colorCodeController;
    [SerializeField] private PieGraph pieGraph;
    
    private CandidateData[] candidates;

    private void Start()
    {
        candidates = Resources.LoadAll<CandidateData>(candidatesFolder);
        if(colorCodeController != null) colorCodeController.CreateCode(candidates);
        if(pieGraph != null) pieGraph.MakeChart(candidates);
    }
}
