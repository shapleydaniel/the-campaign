﻿using System;
using System.Collections.Generic;
using System.Reflection;
using UI_Extensions.Debug_Console;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class DebugController : MonoBehaviour
{
    private bool showConsole;

    private string input;

    public static DebugCommand Kill_All;

    public List<DebugCommandAttribute> commandList;
    
    private void OnGUI()
    {
        if (!showConsole) return;

        var y = 0f;
        GUI.Box(new Rect(0, 0,Screen.width, 30), "");
        GUI.backgroundColor = new Color(0, 0, 0, 0);
        input = GUI.TextField(new Rect(10f, y + 5f, Screen.width-20f, 20f), input);
    }
    
    private void Awake()
    {
       
    }

    public void OnReturn(InputValue value)
    {
        if (!showConsole) return;
        HandleInput();
        input = "";
    }

    public void OnToggleDebug(InputValue value)
    {
        showConsole = !showConsole;
    }

    private void HandleInput()
    {
        foreach (var command in commandList)
        {
            if (input.Contains(command.CommandId))
            {
                var info = UnityEventBase.GetValidMethodInfo(command, command.CommandId, new System.Type[0]);
                info.Invoke(command, null);
            }
        }
        
    }
   
}
