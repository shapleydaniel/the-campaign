﻿using System;

namespace UI_Extensions.Debug_Console
{
    public class DebugCommandBase
    {
        public string CommandId { get; }

        public string CommandDescription { get; }

        public string CommandFormat { get; }

        protected DebugCommandBase(string id, string description, string format)
        {
            CommandId = id;
            CommandDescription = description;
            CommandFormat = format;
        }
    }

    public class DebugCommand : DebugCommandBase
    {
        private Action command;
        
        public DebugCommand(string id, string description, string format, Action command) : base(id, description, format)
        {
            this.command = command;
        }

        public void Invoke()
        {
            command.Invoke();
        }
    }
    
    public class DebugCommand <T>: DebugCommandBase
    {
        private readonly Action<T> command;
        
        public DebugCommand(string id, string description, string format, Action<T> command) : base(id, description, format)
        {
            this.command = command;
        }

        public void Invoke(T value)
        {
            command.Invoke(value);
        }
    }
}