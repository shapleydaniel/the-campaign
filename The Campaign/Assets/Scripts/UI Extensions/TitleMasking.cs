﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

[ExecuteAlways]
public class TitleMasking : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI titleMesh;
    [SerializeField] private Image mask;
    [SerializeField] private float padding;

    private void Awake()
    {
        SetMaskSize();
    }

    private void SetMaskSize()
    {
        if (mask == null && titleMesh == null) return; 
        var newWidth = titleMesh.preferredWidth + (padding*2);
        var size = mask.rectTransform.sizeDelta;
        size.x = newWidth;
        mask.rectTransform.sizeDelta = size;
    }
}
