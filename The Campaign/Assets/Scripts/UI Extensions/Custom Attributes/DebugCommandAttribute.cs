﻿using System;
using UnityEngine;

[AttributeUsage(AttributeTargets.Event)]
public class DebugCommandAttribute : Attribute
{
    public string CommandId { get; }

    public string CommandDescription { get; }
    
    public DebugCommandAttribute(string methodName, string description)
    {
        CommandId = methodName;
        CommandDescription = description;
    }
}
