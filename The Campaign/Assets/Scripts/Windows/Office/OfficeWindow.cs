﻿using System;
using System.Collections.Generic;
using UI_Extensions;
using UnityEngine;
using UnityEngine.AddressableAssets;
using Window;

public class OfficeWindow : WindowBase, IWindowDisplayData
{
    [SerializeField] private AssetReference deskAsset;
    [SerializeField] private Transform parent;

    private Pool<OfficeDesk> pool = new Pool<OfficeDesk>();
    
    public void SetDisplayData<T>(T item)
    {
        var v = item as OfficeData;
        SetOfficeData(v);
    }

    private void SetOfficeData(OfficeData officeData)
    {
        if(pool == null || pool.pool.Count <= 0)
        {
            pool.prefabReference = deskAsset;
            pool.parent = parent;
            pool.CreatePool(officeData.numberDesks, () => AssignDesks(officeData));
            return;
        }

        AssignDesks(officeData);
        
    }

    private void AssignDesks(OfficeData officeData)
    {
       for (var i = 0; i < officeData.numberDesks; i++)
       {
           var desk = pool.GetObject();
           if (i < officeData.employees.Count)
           {
               desk.AddEmployee(officeData.employees[i]);
           }
           else
           {
               desk.RemoveEmployee();
           }

           desk.gameObject.SetActive(true);
       }
    }



}
