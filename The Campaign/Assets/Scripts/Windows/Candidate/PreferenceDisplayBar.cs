﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PreferenceDisplayBar : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI title;
    [SerializeField] private Image bar;

    public void SetPreferenceBarData(PreferenceData preferenceData)
    {
        title.text = preferenceData.preferenceName.ToString();
        //bar.color = preferenceData.color;
        SetScale(preferenceData.weight);
    }

    private void SetScale(float scaleValue)
    {
        var scale = bar.transform.localScale;
        scale.y = scaleValue;
        bar.transform.localScale = scale;
    }
}
