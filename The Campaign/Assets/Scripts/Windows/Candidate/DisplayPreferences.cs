﻿using UnityEngine;
using Window;

public class DisplayDataPreferences : MonoBehaviour, IWindowDisplayData
{
    [SerializeField] private PreferenceDisplayBar preferencePrefabs;
    
    public void SetDisplayData<T>(T item)
    {
        var candidate = item as CandidateData;
        if (candidate == null) return;
        foreach (var preference in candidate.preferences)
        {
            if(preference.preferenceName == PreferenceType.None) continue;
            var prefObject = Instantiate(preferencePrefabs, transform);
            prefObject.name = preference.preferenceName.ToString();
            prefObject.SetPreferenceBarData(preference);
        }
    }
}
