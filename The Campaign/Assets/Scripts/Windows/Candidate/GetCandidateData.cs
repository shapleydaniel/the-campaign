﻿using UnityEngine;

public static class GetCandidateData
{
    private const string SelectedCandidatePath = "Selected";
    private const string AllCandidatePath = "/Candidatte";
    
    public static CandidateData SelectedCandidate()
    {
        var candidates = Resources.LoadAll<CandidateData>(SelectedCandidatePath);
        return candidates[0];
    }

    public static CandidateData[] AllCandidates()
    {
        return Resources.LoadAll<CandidateData>(AllCandidatePath);
    }
}
