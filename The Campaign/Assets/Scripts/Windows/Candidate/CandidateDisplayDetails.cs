﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Window;

public class CandidateDisplayDetails : MonoBehaviour, IWindowDisplayData
{
    [SerializeField] private TextMeshProUGUI nameTextMesh;
    [SerializeField] private TextMeshProUGUI partyTextMesh;
    [SerializeField] private Image characterIcon;
    
    public void SetDisplayData<T>(T item)
    {
        var candidateData = item as CandidateData;
        if (candidateData == null) return;
        nameTextMesh.text = $"Name: {candidateData.name}";
        partyTextMesh.text = $"Party: {candidateData.party.name}";
        characterIcon.sprite = candidateData.idSprite;
    }
}
