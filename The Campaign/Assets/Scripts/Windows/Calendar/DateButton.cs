﻿using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DateButton : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private TextMeshProUGUI textMesh;
    [SerializeField] private Image backgroundImage;

    private CampaignEvent dateEvent;
    private int date;

    private bool active = true;

    public void OnPointerClick(PointerEventData eventData)
    {
        if (!active) return;
       
        Debug.Log("Date Selected, need to figure out events");
    }

    public void SetDetails(int newDate, CampaignEvent newEvent = null, bool deactive = false)
    {
        if (deactive)
        {
            DeactivateDate();
            return;
        }

        date = newDate;
        textMesh.text = newDate.ToString("00");
        if (newEvent != null) 
        {
            AddEvent(newEvent);
        }
    }

    public void AddEvent(CampaignEvent newEvent)
    {
        textMesh.color = Color.white;
        backgroundImage.color = new Color(0.25f, 0.25f, 0.25f);
        dateEvent = newEvent;
    }

    private void DeactivateDate()
    {
        active = false;
        backgroundImage.color = new Color(0.64f, 0.64f, 0.64f, 0.8f);
        textMesh.color = new Color(0.64f, 0.64f, 0.64f);
    }
}
