﻿using UnityEngine;

public enum eventType
{
    none,
    Meeting,
    Press,
    Lobby
}

public class CampaignEvent : ScriptableObject
{
    public eventType type;
    public ScriptableObject eventData;

}
