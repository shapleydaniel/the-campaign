using System;
using System.Collections.Generic;
using UnityEngine;

public class CampaignEventSystem : MonoBehaviour
{
    //TEST
    [SerializeField] private MeetingData meeting;
    
    private Dictionary<int, CampaignEvent> campaignEvents = new Dictionary<int, CampaignEvent>();

    private CampaignEvent currentEvent;

    private void OnEnable()
    {
        var testEvent = ScriptableObject.CreateInstance<CampaignEvent>();
        testEvent.type = eventType.Meeting;
        testEvent.eventData = meeting;
        campaignEvents.Add(060, testEvent);
    }

    private void Update()
    {
        CheckForEvent(GameStats.Instance.ReturnDateTime());
    }

    public void CheckForEvent(int dateTime)
    {
        if (!campaignEvents.ContainsKey(dateTime) && currentEvent == null) return;
        currentEvent = campaignEvents[dateTime];
        switch (currentEvent.type)
        {
            case eventType.none:
                break;
            case eventType.Meeting:
                WindowController.OpenWindow("meeting", currentEvent.eventData);
                currentEvent = null;
                break;
            case eventType.Press:
                break;
            case eventType.Lobby:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }
    
    public void AddEvent(int dateTime, CampaignEvent @event)
    {
        campaignEvents.Add(dateTime, @event);
    }

    public void RemoveEvent(int dateTime)
    {
        campaignEvents.Remove(dateTime);
    }
}
