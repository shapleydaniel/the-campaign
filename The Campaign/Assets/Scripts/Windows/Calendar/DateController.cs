﻿using System;
using UI_Extensions;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class DateController : MonoBehaviour
{
    [SerializeField] private AssetReference buttonPrefab;
    [SerializeField] private Transform parent;
    
    private readonly Pool<DateButton> buttons;

    public DateController(Pool<DateButton> buttons)
    {
        this.buttons = buttons;
    }

    private void OnEnable()
    {
        if (!buttons.initialised)
        {
            buttons.parent = parent == null ? transform : parent;
            buttons.prefabReference = buttonPrefab;
            buttons.CreatePool(31, SetDisplay);
            return;
        }

        SetDisplay();
    }

    private void OnDisable()
    {
        buttons.ResetPool();
    }

    public void SetDisplay()
    {
        buttons.ResetPool();
        var timeData = GameStats.Instance.ReturnTimeData();
        var campaignEvents = timeData.Events;
        var daysInMonth = DateTime.DaysInMonth(DateTime.Now.Year, DateTime.Now.Month);
        for (int i = 0; i < daysInMonth; i++)
        {
            var dateButton = buttons.GetObject();

            var dateEvent = new CampaignEvent();
            if (campaignEvents.ContainsKey(i))
            {
                dateEvent = campaignEvents[i];
            }

            dateButton.SetDetails(i+1, dateEvent.type == eventType.none ? null : dateEvent);
            dateButton.gameObject.SetActive(true);
        }
    }
}
