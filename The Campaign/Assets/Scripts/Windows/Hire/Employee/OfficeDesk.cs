﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(WindowButton))]
public class OfficeDesk : MonoBehaviour, IDropHandler
{
    [SerializeField] private RectTransform progressBar;
    [SerializeField] private Sprite emptyDesk;
    [SerializeField] private Sprite fullDesk;
    [SerializeField] private Image desk;
    
    private int deskLevel = 2;

    public int DeskLevel
    {
        get => deskLevel;
    }

    private WindowButton windowButton;
    private EmployeeData currentEmployee;
    private OfficeData officeData;

    private void Awake()
    {
        windowButton = GetComponent<WindowButton>();
    }

    private void Update()
    {
        if (currentEmployee == null || currentEmployee.currentTasks == null || currentEmployee.currentTasks.Count <= 0) return;
        SetScale(currentEmployee.currentTasks[0].progress);
    }

    private void SetScale(float scaleY)
    {
        if (scaleY < 0) return;
        var scale = progressBar.localScale;
        scale.y = scaleY;
        progressBar.localScale = scale;
    }

    public void AddEmployee(EmployeeData newEmployee)
    {
        if (currentEmployee != null) return;
        currentEmployee = newEmployee;
        desk.sprite = fullDesk;
        windowButton.SetWindowName("Employee");
        windowButton.SetButtonData(newEmployee);
        progressBar.localScale = new Vector2(1, newEmployee.TaskProgress);
    }

    public void RemoveEmployee()
    {
        currentEmployee = null;
        desk.sprite = emptyDesk;
        SetScale(0);
        windowButton.SetWindowName("Hire");
        windowButton.SetButtonData(this);
    }

    public void OnDrop(PointerEventData eventData)
    {
        if (eventData.pointerDrag == null && currentEmployee != null) return;
        var employee = eventData.pointerDrag.GetComponent<EmployeeData>();
        if (employee == null) return;
        AddEmployee(employee);
    }
}
