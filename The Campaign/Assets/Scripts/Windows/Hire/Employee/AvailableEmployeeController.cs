﻿using System.Collections.Generic;
using UnityEngine;
using Window;

public class AvailableEmployeeController : MonoBehaviour, IWindowDisplayData
{
    [SerializeField] private AvailableEmployee availableEmployeePrefab;
    
    private List<AvailableEmployee> employeePool = new List<AvailableEmployee>();
    
    public void SetDisplayData<T>(T item)
    {
        var employees = item as EmployeeData[];
        if (employees == null) return;
        foreach (var employee in employees)
        {
            if(!CheckEmployee(employee)) continue;
            CheckPool(employee);
        }
    }

    private bool CheckEmployee(EmployeeData employeeData)
    {
         return employeePool.Find(x => x.currentEmployee == employeeData) == null;
    }

    private void CheckPool(EmployeeData employeeData)
    {
        foreach (var employee in employeePool)
        {
            if (employee.gameObject.activeSelf) continue;
            employee.SetDetails(employeeData);
            employee.gameObject.SetActive(true);
            return;
        }

        CreateEmployee(employeeData);
    }


    private void CreateEmployee(EmployeeData employeeData)
    {
        var employee = Instantiate(availableEmployeePrefab, transform);
        employee.SetDetails(employeeData);
    }
}
