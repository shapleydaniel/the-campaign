﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Window;

public class EmployeeDetailsDisplay : MonoBehaviour, IWindowDisplayData
{
    [SerializeField] private TextMeshProUGUI nameTextMesh;
    [SerializeField] private TextMeshProUGUI ageTextMesh;
    [SerializeField] private Image characterIcon;
    
    public void SetDisplayData<T>(T item)
    {
        var employeeData = item as EmployeeData;
        if (employeeData == null) return;
        nameTextMesh.text = $"Name: {employeeData.name}";
        ageTextMesh.text = $"Age: {employeeData.age}";
        characterIcon.sprite = employeeData.idImage;
    }
}
