﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AvailableEmployee : Dragable
{
    [SerializeField] private Image id;
    [SerializeField] private TextMeshProUGUI details;
    [SerializeField] private Button hireButton;

    [HideInInspector]public EmployeeData currentEmployee;
    
    public void SetDetails(EmployeeData employeeData)
    {
        currentEmployee = employeeData;
        id.sprite = employeeData.idImage;
        details.text = $"Name: {employeeData.name}";
        hireButton.onClick.AddListener(() =>
        {
            HireWindow.HireEmployee?.Invoke(currentEmployee);
        });
    }

    public void DeactivateEmployee()
    {
        gameObject.SetActive(false);
    }
    
}
