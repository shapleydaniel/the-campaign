﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using Window;

public class HireWindow : MonoBehaviour, IWindowDisplayData
{
    [SerializeField]private Transform parent;
    [SerializeField]private AssetReference employeePrefab;

    private OfficeDesk currentDesk;

    private AvailableEmployee availablePrefab;
    private EmployeesData allEmployees;
    private List<EmployeeData> availableEmployees;

    private List<AvailableEmployee> employeePool = new List<AvailableEmployee>();

    private const int startPool = 6;

    private WindowBase window;

    public static Action<EmployeeData> HireEmployee;

    private void OnEnable()
    {
        HireEmployee += AssignDesk;
    }

    private void OnDisable()
    {
        HireEmployee = null;
        ResetEmployees();
    }

    private void Awake()
    {
        window = GetComponent<WindowBase>();
    }

    public void SetDisplayData<T>(T item)
    {
        var v = item as OfficeDesk;
        SetEmployeesData(v);
    }

    private void SetEmployeesData(OfficeDesk desk)
    {
        currentDesk = desk;
        if (allEmployees == null)
        {
            Addressables.LoadAssetAsync<EmployeesData>("AllEmployees").Completed += OnLoadEmployees;
            return;
        }

        availableEmployees = allEmployees.GetEmployeesAtLevel(desk.DeskLevel);

        if (employeePool.Count <= 0)
        {
            Addressables.LoadAssetAsync<GameObject>(employeePrefab).Completed += OnLoadAvailableEmployee;
            return;
        }

        foreach (var employee in availableEmployees)
        {
            var availableEmployee = CheckPool();
            if (availableEmployee == null) continue;
            availableEmployee.SetDetails(employee);
            availableEmployee.gameObject.SetActive(true);
        }
    }

    private void OnLoadEmployees(AsyncOperationHandle<EmployeesData> obj)
    {
        // In a production environment, you should add exception handling to catch scenarios such as a null result.
        if (obj.Status != AsyncOperationStatus.Succeeded) return;
        allEmployees = obj.Result;
        SetEmployeesData(currentDesk); 
    }

    private void OnLoadAvailableEmployee(AsyncOperationHandle<GameObject> obj)
    {
        // In a production environment, you should add exception handling to catch scenarios such as a null result.
        if (obj.Status != AsyncOperationStatus.Succeeded) return;
        availablePrefab = obj.Result.GetComponent<AvailableEmployee>();
        CreatePool();
    }

    private void CreatePool()
    {
        if (availablePrefab == null) return;
        for(var i = 0; i < startPool; i++)
        {
            var available = Instantiate(availablePrefab, parent);
            available.gameObject.SetActive(false);
            employeePool.Add(available);
        }

        SetEmployeesData(currentDesk);
    }

    private AvailableEmployee CheckPool()
    {
        foreach (var employee in employeePool)
        {
            if (!employee.gameObject.activeSelf)
            {
                return employee;
            }
        }

        return null;
    }

    public void AssignDesk(EmployeeData employee)
    {
        employee.available = false;
        window.CloseWindow();
        currentDesk.AddEmployee(employee);
        OfficeData.HireEmployee(employee);
    }

    private void ResetEmployees()
    {
        foreach (var employee in employeePool)
        {
            employee.gameObject.SetActive(false);
        }
    }
}
