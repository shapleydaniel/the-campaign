using TMPro;
using UnityEngine;

public class TimeCounter : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI textMeshProUGUI;

    private void Update()
    {
        var totalSeconds = GameStats.Instance.ReturnTime();
        var minutes = Mathf.Floor(totalSeconds / 60.0f);
        var seconds = totalSeconds - minutes * 60.0f;
        textMeshProUGUI.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }
}
