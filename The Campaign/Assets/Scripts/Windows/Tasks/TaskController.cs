﻿using System;
using UI_Extensions;
using UnityEngine;
using UnityEngine.AddressableAssets;
using Window;

public class TaskController : MonoBehaviour, IWindowDisplayData
{
    [SerializeField] private AssetReference currentTaskPrefab;
    private const int maxNumberSkills = 8;

    protected  Pool<TaskDisplay> taskPool = new Pool<TaskDisplay>();

    protected EmployeeData currentEmployee;

    public void SetDisplayData<T>(T item)
    {
        if (!(item is EmployeeData employeeData)) return;
        currentEmployee = employeeData;

        if(taskPool.pool.Count <= 0)
        {
            taskPool.parent = transform;
            taskPool.prefabReference = currentTaskPrefab;
            taskPool.CreatePool(maxNumberSkills, () =>
            {
                SetTasks();
            });

            return;
        }
        else
        {
            taskPool.ResetPool();
        }

        SetTasks();
        
    }

    protected virtual void SetTasks()
    {
        foreach (var task in currentEmployee.availableTasks)
        {
            var taskObject = taskPool.GetObject();
            taskObject.SetTask(task);
            taskObject.gameObject.SetActive(true);
        }
    }

    public void TrackEmployee(TaskData taskData)
    {
        currentEmployee.AddTask(taskData);
        TaskQue.trackEmployeeTasks?.Invoke(currentEmployee);
    }
}
