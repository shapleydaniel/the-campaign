﻿using UnityEngine;
using UnityEngine.UI;

public class TaskDisplay : MonoBehaviour
{
    [SerializeField]protected Image icon;
    
    [HideInInspector]public TaskData currentTask;

    protected TaskController taskController;

    public TaskController SetTaskController
    {
        set => taskController = value;
    }
    
    public void SetTask(TaskData taskData)
    {
        if (taskData == null) return;
        currentTask = taskData;
        icon.sprite = taskData.icon;
    }
}
