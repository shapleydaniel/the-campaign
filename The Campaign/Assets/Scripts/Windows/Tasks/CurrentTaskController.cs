﻿using UI_Extensions;

public class CurrentTaskController : TaskController
{
    protected override void SetTasks()
    {
        for (int i = 0; i < currentEmployee.skillLevel; i++)
        {
            var taskObject = (CurrentTask)taskPool.GetObject();
            if (taskObject == null) return;
            taskObject.SetTaskController = this;
            if (currentEmployee.currentTasks.Count > i && currentEmployee.currentTasks[i] != null) 
            {
                taskObject.SetTask(currentEmployee.currentTasks[i]);
            }
            else
            {
                taskObject.SetScale(0);
            }
            taskObject.gameObject.SetActive(true);
        }
    }
}
