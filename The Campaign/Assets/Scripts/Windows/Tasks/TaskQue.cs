﻿using System;
using System.Collections.Generic;
using UnityEngine;

public  class TaskQue : MonoBehaviour
{
    private static readonly List<EmployeeData> employees = new List<EmployeeData>();

    public static Action<EmployeeData> trackEmployeeTasks;

    private void OnEnable()
    {
        trackEmployeeTasks += AddEmployee;
    }

    private void OnDisable()
    {
        trackEmployeeTasks = null;
    }

    private void Update()
    {
        if (employees.Count <= 0) return;
        var removeEmployees = new List<EmployeeData>();
        foreach (var employee in employees)
        {
            UpdateEmployeeTask(employee);
            if (employee.currentTasks.Count <= 0) removeEmployees.Add(employee);
        }

        foreach (var employee in removeEmployees)
        {
            employees.Remove(employee);
        }
    }

    private void UpdateEmployeeTask(EmployeeData employeeData)
    {
        if (employeeData == null) return;
        if (employeeData.currentTasks.Count > 0)
        {
            var task = employeeData.currentTasks[0];
            task.progress -= Time.deltaTime / (employeeData.skillLevel * task.difficulty);
            if (task.progress <= 0)
            {
                employeeData.currentTasks.Remove(task);
                Destroy(task);
            }
        }    
    }

    private void AddEmployee(EmployeeData employeeData)
    {
        employees.Add(employeeData);
    }
}
