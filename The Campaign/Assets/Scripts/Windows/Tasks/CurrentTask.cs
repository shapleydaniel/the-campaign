﻿using UnityEngine;
using UnityEngine.EventSystems;

public class CurrentTask : TaskDisplay, IDropHandler
{
    [SerializeField] private Transform progressBar;

    private void Update()
    {
        if (currentTask != null)
        {
            SetScale(currentTask.progress);
        }
    }

    public void SetScale(float progress)
    {
        if (progress > 0)
        {
            var scale = progressBar.localScale;
            scale.y = progress;
            progressBar.localScale = scale;
        }
        else
        {
            currentTask = null;
        }
    }
    
    public void OnDrop(PointerEventData eventData)
    {
        if (eventData.pointerDrag == null && currentTask != null) return;
        var task = eventData.pointerDrag.GetComponent<TaskDisplay>();
        if (task == null || task.currentTask == null) return;
        var newTask = Instantiate(task.currentTask);
        currentTask = newTask;
        newTask.ResetProgress();
        SetTask(newTask);
        SetScale(1);
        taskController.TrackEmployee(newTask);
        //TaskController.AddTaskToEmployee?.Invoke(newTask);
    }
}
