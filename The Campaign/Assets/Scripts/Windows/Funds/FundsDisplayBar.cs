﻿using GameData;
using UnityEngine;

public class FundsDisplayBar : MonoBehaviour
{
    [SerializeField] private FundStats typeStats;
    [SerializeField] private RectTransform[] bars;

    private RectTransform rectTransform;
    private float[] funds;

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        funds = Resources.Load<FundsData>("Funds").GetFunds(typeStats);
        if (funds == null)
        {
            Debug.Log($"Couldn't Find fund data");
            return;
        }
        SetGraphBars();
    }

    private void SetGraphBars()
    {
        for (var i = 0; i < bars.Length; i++)
        {
            if (funds.Length <= i)
            {
                SetScaleY(bars[i], 0);
                continue;
            }
            
            Debug.Log(rectTransform.rect.height);
            SetScaleY(bars[i], funds[i]/rectTransform.rect.height);
        }
    }

    private static void SetScaleY(RectTransform rect, float value)
    {
        var scale = rect.localScale;
        scale.y = value;
        rect.localScale = scale;
    }
    
}
