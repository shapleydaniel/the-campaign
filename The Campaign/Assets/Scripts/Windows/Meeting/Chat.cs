using System;
using DG.Tweening;
using TMPro;
using UnityEngine;

public class Chat : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI chatText;
    
    [Header("Tween")]
    [SerializeField] private float duration;
    
    public void SetChat(string text, Action onComplete = null)
    {
        //chatText.text = text;
        chatText.DOText("...", 2).onComplete += () =>
        {
            chatText.DOText(text, duration).onComplete += () => onComplete?.Invoke();
        };
    }
}
