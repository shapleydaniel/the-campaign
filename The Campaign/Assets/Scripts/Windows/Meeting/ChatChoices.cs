using System;
using System.Collections.Generic;
using Ink.Runtime;
using UI_Extensions;
using UnityEngine;
using UnityEngine.AddressableAssets;

public class ChatChoices : MonoBehaviour
{
    [SerializeField] private AssetReference prefabReference;
    [SerializeField] private Transform parent;
    [SerializeField] private MeetingController meetingController;
    
    private Pool<ChatChoice> choices = new Pool<ChatChoice>();

    private void OnDisable()
    {
        choices.ResetPool();
    }

    private void InitialisePool(Action onComplete = null)
    {
        choices.parent = parent;
        choices.prefabReference = prefabReference;
        choices.CreatePool(3, onComplete);
    }

    public void SetChoices(List<Choice> newChoices)
    {
        if (!choices.initialised)
        {
            InitialisePool(() =>
            {
                SetChoices(newChoices);
            });
            
            return;
        }
        
        foreach (var choice in newChoices)
        {
            var choiceObject = choices.GetObject();
            if(choiceObject == null) continue;
            choiceObject.SetChoice(choice);
            choiceObject.Choices = this;
            choiceObject.gameObject.SetActive(true);
        }
    }

    public void ResetChats()
    {
        choices.ResetPool();
    }

    public void ChoiceMade(Choice choice)
    {
        meetingController.ChoiceSelected(choice);
    }
}
