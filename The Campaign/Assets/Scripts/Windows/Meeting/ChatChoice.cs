using Ink.Runtime;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class ChatChoice : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private TextMeshProUGUI textMeshProUGUI;

    private ChatChoices choices;

    public ChatChoices Choices
    {
        set => choices = value;
    }

    private Choice currentChoice;

    private bool pressed;

    private void OnDisable()
    {
        pressed = false;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (pressed) return;
        pressed = true;
        choices.ChoiceMade(currentChoice);
    }

    public void SetChoice(Choice choice)
    {
        textMeshProUGUI.text = choice.text;
        currentChoice = choice;
    }
}
