using System;
using System.Collections.Generic;
using System.Linq;
using Ink.Runtime;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using Window;

public class MeetingController : MonoBehaviour, IWindowDisplayData
{
    [SerializeField] private TextAsset scriptAsset;
    [SerializeField] private AssetReference playerAsset;
    [SerializeField] private AssetReference meetingAsset;
    [SerializeField] private Transform parent;
    [SerializeField] private ChatChoices chatChoices;
    [SerializeField] private string playerTag;

    private Story script;
    
    private Chat playerChat;
    private Chat meetingChat;

    private List<GameObject> chats = new List<GameObject>();

    private void OnEnable()
    {
        if (playerChat == null)
        {
            Addressables.LoadAssetAsync<GameObject>(playerAsset).Completed += handle =>
            {
                if (handle.Status != AsyncOperationStatus.Succeeded) return;
                playerChat = handle.Result.GetComponent<Chat>();
            };
        }

        if (playerChat == null)
        {
            Addressables.LoadAssetAsync<GameObject>(meetingAsset).Completed += handle =>
            {
                if (handle.Status != AsyncOperationStatus.Succeeded) return;
                meetingChat = handle.Result.GetComponent<Chat>();
            };
        }
    }

    private void OnDisable()
    {
        if (chats.Count <= 0) return;
        foreach (var chat in chats)
        {
            Destroy(chat);
        }
    }

    private void LoadAssets(Action onComplete)
    {
        if (playerChat == null)
        {
            Addressables.LoadAssetAsync<GameObject>(playerAsset).Completed += handle =>
            {
                if (handle.Status != AsyncOperationStatus.Succeeded) return;
                playerChat = handle.Result.GetComponent<Chat>();
            };
        }

        if (meetingChat == null)
        {
            Addressables.LoadAssetAsync<GameObject>(meetingAsset).Completed += handle =>
            {
                if (handle.Status != AsyncOperationStatus.Succeeded) return;
                meetingChat = handle.Result.GetComponent<Chat>();
                onComplete?.Invoke();
            };
        }
    }

    private void MoveScriptOn()
    {
        if (!script.canContinue) return;
        script.Continue();
        CreateChat(script.currentTags, script.currentText, () =>
        {
            if (script.currentChoices.Count > 0)
            {
                CreateChoices(script.currentChoices);
            }
            else
            {
                MoveScriptOn();
            }
        });
    }

    private void CreateChat(List<string> tags, string currentText, Action onComplete = null)
    {
        var prefabChat = tags.Contains(playerTag) ? playerChat : meetingChat;
        var chat = Instantiate(prefabChat, parent);
        chats.Add(chat.gameObject);
        chat.SetChat(currentText, onComplete);
    }
    
    private void CreateChoices(List<Choice> choices)
    {
        chatChoices.SetChoices(choices);
    }

    public void ChoiceSelected(Choice choice)
    {
        script.ChooseChoiceIndex(choice.index);
        CheckStringTag(script.currentTags);
        chatChoices.ResetChats();
        MoveScriptOn();
    }
    
    private void CheckStringTag(List<string> tags)
    {
        if (tags.Count <= 0) return;
        foreach (var splitTag in from currentTag in tags where currentTag.Contains("adjustment") select currentTag.Split('_'))
        {
            if (splitTag.Length < 2)
            {
                Debug.LogError($"Line: {script.currentText} Tag: {splitTag[1]} Missing section");
                continue;
            }
                
            GameStats.Instance.AdjustStats(splitTag[1], int.Parse(splitTag[2]));
        }
    }

    public void SetDisplayData<T>(T item)
    {
        var meeting = item as MeetingData;
        if (meeting == null) return;
        scriptAsset = meeting.script;
        script = new Story(scriptAsset.text);
        if (meetingChat == null)
        {
            LoadAssets(MoveScriptOn);
            return;
        }
        MoveScriptOn();
    }
}
