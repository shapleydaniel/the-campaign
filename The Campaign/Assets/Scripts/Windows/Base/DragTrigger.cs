﻿using UnityEngine;
using UnityEngine.EventSystems;

public class DragTrigger : MonoBehaviour, IDragHandler
{
    [SerializeField] private Canvas canvas;
    
    [SerializeField] private RectTransform _rectTransform;

    private Vector2 previousPosition;

    public void OnDrag(PointerEventData eventData)
    {
        _rectTransform.anchoredPosition += eventData.delta / canvas.scaleFactor;
    }
}
