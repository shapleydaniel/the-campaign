﻿using UnityEngine;

namespace Window
{
    public class TabController: MonoBehaviour
    {
        [SerializeField] private Tab[] _tabs;
        [SerializeField] private TabPanelController tabPanelController;
        [SerializeField] private float tabHeight;
        private int currentTabSelected;

        private void OnEnable()
        {
            ChangeTab(currentTabSelected);
        }

        private void Awake()
        {
            _tabs = GetComponentsInChildren<Tab>();
        }

        public void ChangeTab(int tabNumber)
        {
            _tabs[currentTabSelected].AdjustTab(false, -tabHeight);
            currentTabSelected = tabNumber;
            if(tabPanelController != null)tabPanelController.OpenPanel(_tabs[currentTabSelected].name);
            _tabs[currentTabSelected].AdjustTab(true, tabHeight);
        }

    }
}