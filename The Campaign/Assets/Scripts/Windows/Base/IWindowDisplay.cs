﻿namespace Window
{
    public interface IWindowDisplayData
    {
        void SetDisplayData<T>(T item);
    }
}