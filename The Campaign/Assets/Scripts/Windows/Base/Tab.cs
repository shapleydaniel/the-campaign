﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using Window;

public class Tab : MonoBehaviour, IPointerClickHandler
{
    [SerializeField] private RectTransform border;
    [SerializeField] private GameObject mask;

    private TabController tabController;

    private void OnEnable()
    {
        tabController = GetComponentInParent<TabController>();
    }

    public void AdjustTab(bool active, float height)
    {
        mask.SetActive(active);
        var size = border.sizeDelta;
        size.y += height;
        border.sizeDelta = size;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (tabController != null) tabController.ChangeTab(gameObject.transform.GetSiblingIndex());
        //TabController.TabSelected?.Invoke(gameObject.transform.GetSiblingIndex());
    }
}
