﻿using System;
using UnityEngine;

public class WindowController : MonoBehaviour
{
    private static Window.WindowBase[] windows;
    
    private void Start()
    {
        GatherWindows();
        CloseAllWindows();
    }

    private void GatherWindows()
    {
        windows = GetComponentsInChildren<Window.WindowBase>(true);
    }
    
    public static void OpenWindow<T>(string windowName, T item)
    {
        windowName = windowName.ToLower();
        foreach (var window in windows)
        {
            if (window.name.ToLower().Contains(windowName))
            {
                window.OpenWindow(item);
            }
        }
    }

    public static void OpenWindow(string windowName)
    {
        windowName = windowName.ToLower();
        foreach (var window in windows)
        {
            if (window.name.ToLower().Contains(windowName))
            {
                window.OpenWindow();
            }
        }

    }

    public void CloseAllWindows()
    {
        foreach (var window in windows)
        {
            window.CloseWindow();
        }
    }
}
