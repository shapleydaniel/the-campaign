﻿using UnityEngine;

namespace Window
{
    public class WindowBase : MonoBehaviour
    {
        private ScriptableObject scriptableObject;
        private Vector2 previousPosition;

        private IWindowDisplayData[] interfaces;

        private void OnEnable()
        {
            if (interfaces != null) return;
            interfaces = GetComponentsInChildren<IWindowDisplayData>(true);
        }

        public virtual void OpenWindow<T>(T item)
        {
            gameObject.SetActive(true);
            transform.position = previousPosition;
            if(item == null) return;
            FindAllDisplays(item);
        }

        public void OpenWindow()
        {
            gameObject.SetActive(true);
            transform.position = previousPosition;
        }

        public void CloseWindow()
        {
            previousPosition = transform.position;
            gameObject.SetActive(false);
        }
    
        private void FindAllDisplays<T>(T item)
        {
            if (interfaces == null) return;
            foreach (var windowDisplay in interfaces)
            {
                windowDisplay.SetDisplayData(item);
            }
        }
    }
}


