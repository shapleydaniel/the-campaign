﻿using System.Collections.Generic;
using UnityEngine;

public class TabPanelController : MonoBehaviour
{
    private List<GameObject> panelWindows = new List<GameObject>();
    private GameObject previousPanel;

    private void Start()
    {
        foreach (Transform child in transform)
        {
            panelWindows.Add(child.gameObject);
            child.gameObject.SetActive(false);
        }
    }

    public void OpenPanel(string panelName)
    {
        panelName = panelName.ToLower();
        foreach (var panel in panelWindows)
        {
            var name = panel.name.ToLower();
            if (!name.Contains(panelName)) continue;
            if(previousPanel != null)previousPanel.SetActive(false);
            previousPanel = panel;
            panel.SetActive(true);
        }
    }
}
